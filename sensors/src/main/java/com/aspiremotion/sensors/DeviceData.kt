/**
 * DeviceData.kt
 *
 * Copyright 2016-2022 Booguu Company Limited. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights granted under the terms of a software
 * license agreement between the user who downloaded the software, his/her employer (which must be your
 * employer) and Booguu Co. Ltd., (the "License").  You may not use this Software unless you agree to abide by the
 * terms of the License which can be found at www.booguu.bio/licenseterms.  The License limits your use, and you
 * acknowledge, that the Software may be modified, copied, and distributed when used in conjunction with an
 * Booguu Co. Ltd., product.  Other than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this Software and/or its documentation for any
 * purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY
 * OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL BOOGUU OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT,
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software, contact Booguu via email:
 * info@booguu.bio.
 */

package com.aspiremotion.sensors

enum class DeviceDataType {
    ACC, GYRO, MAG, QUATERNION
}

/**
 * Base class for reporting device sensor data or download progress
 */
abstract class DeviceData

/**
 * Class for streaming/logging data
 *
 * @property acceleration Accelerometer's acceleration data (g)
 *
 * @property gyro Gyroscope's angular velocity data (deg/s)
 *
 * @property magneticField Magnetometer's magnetic field data (T)
 *
 * @property quaternion Sensor Fusion quaternion data
 */
data class DeviceSensorData(
    val acceleration: CoordinateTimeData?,
    val gyro: CoordinateTimeData?,
    val magneticField: CoordinateTimeData?,
    val quaternion: CoordinateTimeData?
) : DeviceData()

/**
 * Class for reporting logged data download progress
 *
 * @property total Total number of entries logged
 *
 * @property remain Remaining number of entries to be downloaded
 */
data class DownloadProgress(val total: Int, val remain: Int) : DeviceData()