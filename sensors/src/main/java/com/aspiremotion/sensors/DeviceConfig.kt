/**
 * DeviceConfig.kt
 *
 * Copyright 2016-2022 Booguu Company Limited. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights granted under the terms of a software
 * license agreement between the user who downloaded the software, his/her employer (which must be your
 * employer) and Booguu Co. Ltd., (the "License").  You may not use this Software unless you agree to abide by the
 * terms of the License which can be found at www.booguu.bio/licenseterms.  The License limits your use, and you
 * acknowledge, that the Software may be modified, copied, and distributed when used in conjunction with an
 * Booguu Co. Ltd., product.  Other than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this Software and/or its documentation for any
 * purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY
 * OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL BOOGUU OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT,
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software, contact Booguu via email:
 * info@booguu.bio.
 */

package com.aspiremotion.sensors

/**
 * Presets for magnetometer module.
 */
enum class MagnetometerPreset {
    /**
     * Preset: Low Power
     *
     * ODR: 10Hz
     *
     * Average Current: 170µA
     *
     * Noise: 1.0µT (xy axis), 1.4µT (z axis)
     */
    LOW_POWER,

    /**
     * Preset: Regular
     *
     * ODR: 10Hz
     *
     * Average Current: 0.5mA
     *
     * Noise: 0.6µT
     */
    REGULAR,

    /**
     * Preset: Enhanced Regular
     *
     * ODR: 10Hz
     *
     * Average Current: 0.8mA
     *
     * Noise: 0.5µT
     */
    ENHANCED_REGULAR,

    /**
     * Preset: High Accuracy
     *
     * ODR: 20Hz
     *
     * Average Current: 4.9mA
     *
     * Noise: 0.3µT
     */
    HIGH_ACCURACY
}

/**
 * Configuration that contains settings for Accelerometer, Gyroscope, Magnetometer.
 *
 * @property accConfig Configuration for Accelerometer module.
 *
 * @property gyroConfig Configuration for Gyroscope module.
 *
 * @property magConfig Configuration for Magnetometer module.
 *
 * @property types List of enabled module types.
 *
 * @property odr ODR of module with highest output rate.
 */
open class DeviceConfig(
    val accConfig: AccConfig?,
    val gyroConfig: GyroConfig?,
    val magConfig: MagConfig?
) {
    open val types: List<DeviceDataType>
        get() {
            val list = mutableListOf<DeviceDataType>()
            if (accConfig != null) list.add(DeviceDataType.ACC)
            if (gyroConfig != null) list.add(DeviceDataType.GYRO)
            if (magConfig != null) list.add(DeviceDataType.MAG)
            return list
        }

    open val odr: Int
        get() {
            val odrAcc = accConfig?.odr ?: 0
            val odrGyro = gyroConfig?.odr ?: 0
            val odrMag = when (magConfig?.preset) {
                MagnetometerPreset.LOW_POWER -> 10
                MagnetometerPreset.REGULAR -> 10
                MagnetometerPreset.ENHANCED_REGULAR -> 10
                MagnetometerPreset.HIGH_ACCURACY -> 20
                else -> 0
            }
            return arrayListOf(odrAcc, odrGyro, odrMag).max()
        }
}

/**
 * Configuration that enables Sensor Fusion for Quaternion module.
 *
 * @property acc Enables Accelerometer along with Quaternion.
 *
 * @property gyro Enables Gyroscope along with Quaternion.
 *
 * @property mag Enables Magnetometer along with Quaternion.
 *
 * @param accRange: Range of Accelerometer module.
 *
 * @param gyroRange: Range of Gyroscope module.
 *
 * @property odr ODR with Sensor Fusion is always 100Hz.
 */
open class QuaternionDeviceConfig(
    val acc: Boolean,
    val gyro: Boolean,
    val mag: Boolean,
    accRange: Int,
    gyroRange: Int
) : DeviceConfig(
    AccConfig(100, accRange),
    GyroConfig(100, gyroRange),
    null
) {
    override val types: List<DeviceDataType>
        get() {
            val list = mutableListOf<DeviceDataType>()
            if (acc) list.add(DeviceDataType.ACC)
            if (gyro) list.add(DeviceDataType.GYRO)
            if (mag) list.add(DeviceDataType.MAG)
            list.add(DeviceDataType.QUATERNION)
            return list
        }

    override val odr: Int
        get() { return 100 }
}

/**
 * Accelerometer module configuration
 *
 * @property odr ODR (Hz) for Accelerometer. Valid ODR: 25, 50, 100.
 *
 * @property range Range (G) for Accelerometer. Valid range: 2, 4, 8, 16.
 */
class AccConfig(val odr: Int, val range: Int)

/**
 * Gyroscope module configuration
 *
 * @property odr ODR (Hz) for Gyroscope.
 *
 * Valid ODR: 25, 50, 100.
 *
 * @property range Range (DPS) for Accelerometer.
 *
 * Valid range: 125, 250, 500, 1000, 2000.
 */
class GyroConfig(val odr: Int, val range: Int)

/**
 * Magnetometer module configuration
 *
 * @property preset Magnetometer preset
 */
class MagConfig(val preset: MagnetometerPreset)