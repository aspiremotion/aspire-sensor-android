/**
 * DeviceManager.kt
 *
 * Copyright 2016-2022 Booguu Company Limited. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights granted under the terms of a software
 * license agreement between the user who downloaded the software, his/her employer (which must be your
 * employer) and Booguu Co. Ltd., (the "License").  You may not use this Software unless you agree to abide by the
 * terms of the License which can be found at www.booguu.bio/licenseterms.  The License limits your use, and you
 * acknowledge, that the Software may be modified, copied, and distributed when used in conjunction with an
 * Booguu Co. Ltd., product.  Other than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this Software and/or its documentation for any
 * purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY
 * OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL BOOGUU OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT,
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software, contact Booguu via email:
 * info@booguu.bio.
 */

package com.aspiremotion.sensors

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.os.ParcelUuid
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.onFailure
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import timber.log.Timber
import kotlin.math.pow

/**
 * Device Manager interface simplifies BLE scanning for specific Device type without tbe need
 * of understanding lower level Bluetooth APIs.
 *
 * @property scanStartTime Timestamp for last time BLE scan started.
 *
 * @property signals Cache for discovered devices and history of its RSSI values
 *
 * @property distances Cache for discovered devices and converted distance based on RSSI
 *
 * @property devices Flow of newly discovered devices with RSSI after calling [startBleScan].
 *
 */
interface DeviceManager {

    val scanStartTime: Long

    val signals: Map<Device, MutableList<Pair<Long, Int>>>

    val distances: Map<Device, Double>

    val devices: Flow<Pair<Device, Int>>

    /**
     * Start BLE scan. Discovered devices can be retrieved from [devices].
     */
    suspend fun startBleScan()

    /**
     * Stop BLE scan.
     */
    suspend fun stopBleScan()

    /**
     * Retrieve device using BluetoothDevice API.
     */
    fun getDevice(bluetoothDevice: BluetoothDevice): Device

    /**
     * Retrieve device using MAC address. Must be called after Device Manager is enabled.
     */
    fun getDevice(address: String): Device
}

abstract class BleDeviceManager(
    val serviceUuid: String
) : DeviceManager {

    protected abstract val bluetoothAdapter: BluetoothAdapter?

    override var scanStartTime: Long = 0

    override val signals = mutableMapOf<Device, MutableList<Pair<Long, Int>>>()

    override val distances = mutableMapOf<Device, Double>()

    private val cache = mutableMapOf<BluetoothDevice, Device>()

    protected val devicesMutableSharedFlow = MutableSharedFlow<Pair<Device, Int>>()

    override val devices: Flow<Pair<Device, Int>>
        get() = devicesMutableSharedFlow.asSharedFlow()

    private val bleScanner by lazy {
        bluetoothAdapter?.bluetoothLeScanner
    }

    protected var scanJob: Job? = null

    protected open val scanFilters: ArrayList<ScanFilter>
        get() = arrayListOf(
            ScanFilter.Builder().setServiceUuid(
                ParcelUuid.fromString(serviceUuid)
            ).build()
        )

    private val scanSettings = ScanSettings.Builder()
        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
        .build()

    protected fun lookup(bluetoothDevice: BluetoothDevice, rssi: Int): Pair<Device, Int> {
        val current = cache[bluetoothDevice] ?: getDevice(bluetoothDevice)
        cache[bluetoothDevice] = current
        Timber.d("Found $current with RSSI: $rssi")
        cache(current, rssi)
        return Pair(current, rssi)
    }

    protected fun cache(device: Device, rssi: Int) {
        val window = 5000000000
        val now = System.nanoTime()
        (signals[device] ?: mutableListOf())
            .apply {
                add(Pair(now, rssi))
            }.also { list ->
                signals[device] = list
                distances[device] = list
                    .filter { (now - it.first) < window }
                    .let { it.fold(0.0) { sum, pair -> sum + pair.second } / list.count() }
                    .let { 10.0.pow((-59.0 - it) / 20.0) }
            }
    }

    override suspend fun startBleScan() {
        scanJob?.cancelAndJoin()

        signals.clear()
        distances.clear()
        scanStartTime = System.nanoTime()
        scanJob = scanCallback().onEach {
            devicesMutableSharedFlow.emit(lookup(it.first, it.second))
        }.launchIn(CoroutineScope(currentCoroutineContext()))
    }

    override suspend fun stopBleScan() {
        scanJob?.cancelAndJoin()
    }

    // Used to connect to device by MAC address (e.g. remembered device)
    override fun getDevice(address: String): Device {
        return bluetoothAdapter?.let { getDevice(it.getRemoteDevice(address)) }
            ?: throw IllegalStateException("Can't use $this without BluetoothAdapter!")
    }

    private fun scanCallback(): Flow<Pair<BluetoothDevice, Int>> = callbackFlow {

        val callback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult) {
                trySendBlocking(Pair(result.device, result.rssi))
                    .onFailure {
                        Timber.e("onScanResult: $it")
                    }
            }
        }

        try {
            bleScanner?.startScan(scanFilters, scanSettings, callback)
        } catch (error: SecurityException) {
            Timber.e("startScan: $error")
        }

        awaitClose {
            try {
                bleScanner?.stopScan(callback)
            } catch (error: SecurityException) {
                Timber.e("stopScan: $error")
            }
        }
    }
}