/**
 * AspireBoard.kt
 *
 * Copyright 2016-2022 Booguu Company Limited. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights granted under the terms of a software
 * license agreement between the user who downloaded the software, his/her employer (which must be your
 * employer) and Booguu Co. Ltd., (the "License").  You may not use this Software unless you agree to abide by the
 * terms of the License which can be found at www.booguu.bio/licenseterms.  The License limits your use, and you
 * acknowledge, that the Software may be modified, copied, and distributed when used in conjunction with an
 * Booguu Co. Ltd., product.  Other than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this Software and/or its documentation for any
 * purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY
 * OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL BOOGUU OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT,
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software, contact Booguu via email:
 * info@booguu.bio.
 */

@file:OptIn(ExperimentalUnsignedTypes::class)

package com.aspiremotion.sensors.aspire

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGatt.GATT_SUCCESS
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothProfile.STATE_CONNECTED
import android.bluetooth.BluetoothProfile.STATE_DISCONNECTED
import android.content.Context
import android.os.Build
import com.aspiremotion.sensors.*
import com.aspiremotion.sensors.CoordinateData
import com.aspiremotion.sensors.CoordinateTimeData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.onFailure
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeoutOrNull
import timber.log.Timber
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*
import kotlin.time.DurationUnit
import kotlin.time.toDuration

interface AspireDeviceCharacteristics {

    val service: String

    val tx: String

    val rx: String
}

enum class AspireDeviceServiceType(override val service: String) : AspireDeviceCharacteristics {

    ASPIRE_BALANCE("FF7EC6E3-9D72-4252-8372-1E56B63A2D04") {
        override val tx = "FF7EC6E4-9D72-4252-8372-1E56B63A2D04"
        override val rx = "FF7EC6E9-9D72-4252-8372-1E56B63A2D04"
    },
}

class AspireBoard(
    val type: AspireDeviceServiceType,
    private val device: BluetoothDevice,
    private val context: Context
) : BluetoothGattCallback() {

    companion object {
        const val CLIENT_CHARACTERISTIC_CONFIGURATION = "00002902-0000-1000-8000-00805f9b34fb"
        const val BATTERY_SERVICE = "0000180F-0000-1000-8000-00805F9B34FB"
        const val BATTERY_LEVEL = "00002A19-0000-1000-8000-00805F9B34FB"
        const val DEVICE_INFORMATION = "0000180A-0000-1000-8000-00805F9B34FB"
        const val FIRMWARE_REVISION = "00002A26-0000-1000-8000-00805F9B34FB"
    }

    val address: String
        get() = device.address

    private var gatt: BluetoothGatt? = null

    private val requireGatt: BluetoothGatt
        get() = gatt ?: throw IllegalStateException("GATT service not available.")

    private val gattMutex = Mutex()

    private var connection: Job? = null

    private lateinit var descriptorWriteChannel: Channel<GattDescriptorWrite>

    private lateinit var servicesDiscoveredChannel: Channel<GattServicesDiscovered>

    private lateinit var characteristicReadWriteChannel: Channel<GattCharacteristicReadWrite>

    private val characteristicChangeFlow = MutableSharedFlow<GattCharacteristicChanged>()

    private val isGattConnectedMutableStateFlow = MutableStateFlow(false)

    private var isGattConnected: Boolean
        get() = isGattConnectedMutableStateFlow.value
        private set(value) { isGattConnectedMutableStateFlow.value = value }

    private val stateMutableSharedFlow = MutableSharedFlow<DeviceConnectionState>(replay = 1)

    val state: SharedFlow<DeviceConnectionState>
        get() = stateMutableSharedFlow.asSharedFlow()

    val isDisconnected: Flow<DeviceConnectionState>
        get() = stateMutableSharedFlow.filter {
            it == DeviceConnectionState.EXPECTED_DISCONNECTED || it == DeviceConnectionState.UNEXPECTED_DISCONNECTED
        }

    private var isUserInitiatedDisconnect = false

    private fun requireCharacteristic(
        service: String,
        characteristic: String
    ): BluetoothGattCharacteristic {
        return requireGatt.getService(UUID.fromString(service))
            ?.getCharacteristic(UUID.fromString(characteristic))
            ?: throw IllegalStateException("Service characteristic not available.")
    }

    suspend fun connect(timeout: Long) {
        gattMutex.withLock {
            if (gatt != null) {
                Timber.d("Already connected to Aspire Board $this")
                return
            }
            isUserInitiatedDisconnect = false
            isGattConnected = false
            connection = connectGatt().onEach {
                when (it) {
                    is GattServicesDiscovered -> onServicesDiscovered(it)
                    is GattDescriptorWrite -> onDescriptorWrite(it)
                    is GattCharacteristicReadWrite -> onCharacteristicReadWrite(it)
                    is GattCharacteristicChanged -> onCharacteristicChanged(it)
                }
            }.onStart {
                descriptorWriteChannel = Channel()
                servicesDiscoveredChannel = Channel()
                characteristicReadWriteChannel = Channel()
            }.onCompletion {
                withContext(NonCancellable) {
                    descriptorWriteChannel.cancel()
                    servicesDiscoveredChannel.cancel()
                    characteristicReadWriteChannel.cancel()
                    try {
                        gatt?.apply {
                            if (isGattConnected) {
                                disconnect()
                            }
                            close()
                        }
                    } catch (_: SecurityException) {
                    }
                    gatt = null
                    stateMutableSharedFlow.emit(
                        if (isUserInitiatedDisconnect)
                            DeviceConnectionState.EXPECTED_DISCONNECTED
                        else
                            DeviceConnectionState.UNEXPECTED_DISCONNECTED
                    )
                }
            }.launchIn(CoroutineScope(currentCoroutineContext()))

            val connected = withTimeoutOrNull(timeout.toDuration(DurationUnit.SECONDS)) {
                isGattConnectedMutableStateFlow.first { connected -> connected }
            }
            if (connected == null) {
                try {
                    gatt?.apply {
                        disconnect()
                        close()
                    }
                    gatt = null
                } catch (_: SecurityException) {
                }
                throw ConnectionTimeoutException()
            }
        }

        discoverServices()

        setCharacteristicNotification()

        stateMutableSharedFlow.emit(DeviceConnectionState.CONNECTED)
    }

    suspend fun disconnect() {
        gattMutex.withLock {
            try {
                gatt?.disconnect()
            } catch (_: SecurityException) {
                throw BluetoothPermissionException()
            }
            isDisconnected.first()
        }
    }

    suspend fun getBatteryLevel(): Int {
        return read(
            BATTERY_SERVICE,
            BATTERY_LEVEL
        )[0].toInt()
    }

    suspend fun getFirmwareRevision(): String {
        return read(
            DEVICE_INFORMATION,
            FIRMWARE_REVISION
        ).decodeToString()
    }

    suspend fun configureAccelerometer(odr: Int, range: Int) {
        when (odr) {
            25 -> write(AspireCommandCode.SET_ACCELEROMETER_ODR_25HZ)
            50 -> write(AspireCommandCode.SET_ACCELEROMETER_ODR_50HZ)
            100 -> write(AspireCommandCode.SET_ACCELEROMETER_ODR_100HZ)
        }
        when (range) {
            2 -> write(AspireCommandCode.SET_ACCELEROMETER_RANGE_2G)
            4 -> write(AspireCommandCode.SET_ACCELEROMETER_RANGE_4G)
            8 -> write(AspireCommandCode.SET_ACCELEROMETER_RANGE_8G)
            16 -> write(AspireCommandCode.SET_ACCELEROMETER_RANGE_16G)
        }
    }

    suspend fun configureGyro(odr: Int, range: Int) {
        when (odr) {
            25 -> write(AspireCommandCode.SET_GYRO_ODR_25HZ)
            50 -> write(AspireCommandCode.SET_GYRO_ODR_50HZ)
            100 -> write(AspireCommandCode.SET_GYRO_ODR_100HZ)
        }
        when (range) {
            125 -> write(AspireCommandCode.SET_GYRO_RANGE_125DPS)
            250 -> write(AspireCommandCode.SET_GYRO_RANGE_250DPS)
            500 -> write(AspireCommandCode.SET_GYRO_RANGE_500DPS)
            1000 -> write(AspireCommandCode.SET_GYRO_RANGE_1000DPS)
            2000 -> write(AspireCommandCode.SET_GYRO_RANGE_2000DPS)
        }
    }

    suspend fun configureMagnetometer(preset: MagnetometerPreset) {
        when (preset) {
            MagnetometerPreset.LOW_POWER -> write(AspireCommandCode.SET_MAGNETOMETER_LOW_POWER)
            MagnetometerPreset.REGULAR -> write(AspireCommandCode.SET_MAGNETOMETER_REGULAR)
            MagnetometerPreset.ENHANCED_REGULAR -> write(AspireCommandCode.SET_MAGNETOMETER_ENHANCED_REGULAR)
            MagnetometerPreset.HIGH_ACCURACY -> write(AspireCommandCode.SET_MAGNETOMETER_HIGH_ACCURACY)
        }
    }

    suspend fun configureModules(accelerometer: Boolean, gyro: Boolean, magnetometer: Boolean) {
        write(if (accelerometer) AspireCommandCode.SET_ACCELEROMETER_ON else AspireCommandCode.SET_ACCELEROMETER_OFF)
        write(if (gyro) AspireCommandCode.SET_GYRO_ON else AspireCommandCode.SET_GYRO_OFF)
        write(if (magnetometer) AspireCommandCode.SET_MAGNETOMETER_ON else AspireCommandCode.SET_MAGNETOMETER_OFF)
    }

    suspend fun configureSensorFusion(enabled: Boolean) {
        write(if (enabled) AspireCommandCode.SET_SENSOR_FUSION_ON else AspireCommandCode.SET_SENSOR_FUSION_OFF)
    }

    suspend fun configureQuaternionStream(enabled: Boolean) {
        write(if (enabled) AspireCommandCode.SET_QUATERNION_ON else AspireCommandCode.SET_QUATERNION_OFF)
    }

    suspend fun configureLinearAcceleration(enabled: Boolean) {
        write(if (enabled) AspireCommandCode.SET_LINEAR_ACCELERATION_ON else AspireCommandCode.SET_LINEAR_ACCELERATION_OFF)
    }

    suspend fun setStreaming(enabled: Boolean) {
        if (enabled) write(AspireCommandCode.SET_STREAMING_ON) else write(AspireCommandCode.SET_STREAMING_OFF)
    }

    suspend fun setLogging(enabled: Boolean) {
        if (enabled) write(AspireCommandCode.SET_LOGGING_ON) else write(AspireCommandCode.SET_LOGGING_OFF)
    }

    suspend fun setDownload(enabled: Boolean) {
        if (enabled) write(AspireCommandCode.SET_DOWNLOAD_ON) else write(AspireCommandCode.SET_DOWNLOAD_OFF)
    }

    fun receiveStream(): Flow<DeviceSensorData> = callbackFlow {
        characteristicChangeFlow.filter { it.value.isSetStreamOnCommand }.onEach {
            // Command (2 Bytes), Flags (1 Byte)
            val size = 3
            val segments = splitSegments(
                it.value,
                AspireCommandCode.SET_STREAMING_ON.hexValue.asList(),
                size
            )
            segments.map { segment ->
                convert(
                    segment.dropLast(size).toByteArray(),
                    segment.last().toUByte()
                )
            }.forEach { data -> send(data) }
        }.collect()
    }

    fun receiveDownload(): Flow<Pair<DeviceSensorData, Int>> = callbackFlow {
        characteristicChangeFlow.filter { it.value.isSetDownloadOnCommand }
            .collect {
                // Command (2 Bytes), Flags (1 Byte), Remaining (4 Bytes)
                val size = 7
                val segments = splitSegments(
                    it.value,
                    AspireCommandCode.SET_DOWNLOAD_ON.hexValue.asList(),
                    size
                )
                segments.map { segment ->
                    Pair(
                        convert(
                            segment.dropLast(size).toByteArray(),
                            segment[segment.size - 5].toUByte()
                        ),
                        segment.toByteArray().toInt(segment.size - 4, ByteOrder.BIG_ENDIAN)
                    )
                }.forEach { data -> send(data) }
            }
    }

    suspend fun checkLogEntries(): Int {
        return write(AspireCommandCode.CHECK_LOG_ENTRIES).toInt(0, ByteOrder.BIG_ENDIAN)
    }

    suspend fun resetLogEntries() {
        write(AspireCommandCode.RESET_LOG_ENTRIES)
    }

    suspend fun disableModules() {
        configureQuaternionStream(false)
        configureSensorFusion(false)
        configureModules(accelerometer = false, gyro = false, magnetometer = false)
    }

    suspend fun readCalibrationStatus(): Boolean {
        return CalibrationStatus.CALIBRATED.hexValue.contentEquals(
            write(AspireCommandCode.CALIBRATE_STATUS)
        )
    }

    suspend fun configureTxPower(strength: Int) {
        when (strength) {
            -16 -> write(AspireCommandCode.SET_TX_POWER_0)
            -8 -> write(AspireCommandCode.SET_TX_POWER_1)
            -4 -> write(AspireCommandCode.SET_TX_POWER_2)
            0 -> write(AspireCommandCode.SET_TX_POWER_3)
            4 -> write(AspireCommandCode.SET_TX_POWER_4)
        }
    }

    suspend fun configureLedPattern(
        color: LedColorCode,
        highTime: UInt,
        pulse: UInt,
        repeat: UInt
    ) {
        var command = AspireCommandCode.SET_LED_HIGH_TIME.hexValue
        command += color.hexValue
        command += highTime.toByteArray()
        write(type.service, type.tx, command)

        command = AspireCommandCode.SET_LED_PULSE_DURATION.hexValue
        command += color.hexValue
        command += pulse.toByteArray()
        write(type.service, type.tx, command)

        command = AspireCommandCode.SET_LED_REPEAT_COUNT.hexValue
        command += color.hexValue
        command += repeat.toByteArray()
        write(type.service, type.tx, command)
    }

    suspend fun playLed() {
        write(AspireCommandCode.PLAY_LED)
    }

    suspend fun stopLed() {
        write(AspireCommandCode.STOP_LED)
    }

    suspend fun resetLed() {
        write(AspireCommandCode.RESET_LED)
    }

    suspend fun reset() {
        isUserInitiatedDisconnect = true
        setStreaming(false)
        setLogging(false)
        setDownload(false)
        disableModules()
        resetLogEntries()
        write(AspireCommandCode.RESET)
    }

    private suspend fun connectGatt(): Flow<GattCallbackResult> = callbackFlow {

        val callback = object : BluetoothGattCallback() {

            override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
                when {
                    status == GATT_SUCCESS && newState == STATE_CONNECTED -> {
                        isGattConnected = true
                    }
                    status == GATT_SUCCESS && newState == STATE_DISCONNECTED -> {
                        isGattConnected = false
                        close()
                    }
                    status != GATT_SUCCESS -> {
                        isGattConnected = false
                        close()
                    }
                }
                Timber.v("onConnectionStateChange, State: $newState, Status: $status")
            }

            override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
                trySendBlocking(GattServicesDiscovered(status))
                    .onFailure { throwable ->
                        Timber.e("onServicesDiscovered: $throwable")
                    }
            }

            override fun onDescriptorWrite(
                gatt: BluetoothGatt,
                descriptor: BluetoothGattDescriptor,
                status: Int
            ) {
                trySendBlocking(GattDescriptorWrite(descriptor.value.copyOf(), status))
                    .onFailure { throwable ->
                        Timber.e("onDescriptorWrite: $throwable")
                    }
            }

            override fun onCharacteristicRead(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic,
                status: Int
            ) {
                trySendBlocking(GattCharacteristicReadWrite(characteristic.value.copyOf(), status))
                    .onFailure { throwable ->
                        Timber.e("onCharacteristicRead: $throwable")
                    }
            }

            override fun onCharacteristicWrite(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic,
                status: Int
            ) {
                trySendBlocking(GattCharacteristicReadWrite(characteristic.value.copyOf(), status))
                    .onFailure { throwable ->
                        Timber.e("onCharacteristicWrite: $throwable")
                    }
            }

            override fun onCharacteristicChanged(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic
            ) {
                trySendBlocking(GattCharacteristicChanged(characteristic.value.copyOf()))
                    .onFailure { throwable ->
                        Timber.e("onCharacteristicChanged: $throwable")
                    }
            }
        }

        gatt = try {
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> device.connectGatt(
                    context,
                    false,
                    callback,
                    BluetoothDevice.TRANSPORT_LE,
                    BluetoothDevice.PHY_LE_1M_MASK
                )
                else -> device.connectGatt(
                    context,
                    false,
                    callback,
                    BluetoothDevice.TRANSPORT_LE
                )
            }
        } catch (_: SecurityException) {
            throw BluetoothPermissionException()
        }

        awaitClose {  }
    }

    private suspend fun onServicesDiscovered(result: GattServicesDiscovered) {
        servicesDiscoveredChannel.send(result)
    }

    private suspend fun onDescriptorWrite(result: GattDescriptorWrite) {
        descriptorWriteChannel.send(result)
    }

    private suspend fun onCharacteristicReadWrite(result: GattCharacteristicReadWrite) {
        when {
            result.value.contentEquals(AspireCommandCode.CHECK_LOG_ENTRIES.hexValue) ->
                if (result.status != GATT_SUCCESS) characteristicReadWriteChannel.send(result) else return
            result.value.contentEquals(AspireCommandCode.CALIBRATE_STATUS.hexValue) ->
                if (result.status != GATT_SUCCESS) characteristicReadWriteChannel.send(result) else return
            else -> characteristicReadWriteChannel.send(result)
        }
    }

    private suspend fun onCharacteristicChanged(result: GattCharacteristicChanged) {
        val value = result.value
        when {
            value.isCalibratedCommand ->
                characteristicReadWriteChannel.send(
                    GattCharacteristicReadWrite(
                        result.value,
                        0
                    )
                )
            value.isNotCalibratedCommand ->
                characteristicReadWriteChannel.send(
                    GattCharacteristicReadWrite(
                        result.value,
                        0
                    )
                )
            value.isCheckLogEntriesCommand ->
                characteristicReadWriteChannel.send(
                    GattCharacteristicReadWrite(
                        result.value,
                        0
                    )
                )
            value.isSetStreamOnCommand -> characteristicChangeFlow.emit(result)
            value.isSetDownloadOnCommand -> characteristicChangeFlow.emit(result)
        }
    }

    private suspend fun discoverServices() {
        Timber.v("discoverServices")
        val response = request(servicesDiscoveredChannel) {
            try {
                discoverServices()
            } catch (_: SecurityException) {
                throw BluetoothPermissionException()
            }
        }
        if (response.status != GATT_SUCCESS) {
            throw GattException("Unexpected onServicesDiscovered ${response.status}")
        }
        gatt?.services?.forEach {
            Timber.d("Service UUID: ${it.uuid}")
        }
    }

    private suspend fun setCharacteristicNotification() {
        Timber.v("setCharacteristicNotification")
        val gatt = requireGatt
        val characteristic = requireCharacteristic(type.service, type.rx)
        if ((characteristic.properties and BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0) {
            val descriptor =
                characteristic.getDescriptor(
                    UUID.fromString(
                        CLIENT_CHARACTERISTIC_CONFIGURATION
                    )
                )
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            try {
                if (gatt.setCharacteristicNotification(characteristic, true).not()) {
                    throw GattException("Characteristic ${type.rx} cannot be notified.")
                }
                val response = request(
                    descriptorWriteChannel,
                    "Characteristic ${type.rx} cannot be notified."
                ) {
                    gatt.writeDescriptor(descriptor)
                }
                validate(response.status, response.value, "writeDescriptor")
            } catch (_: SecurityException) {
                throw BluetoothPermissionException()
            }
        } else {
            throw GattException("Characteristic ${type.rx} cannot be notified.")
        }
    }

    private suspend fun read(service: String, uuid: String): ByteArray {
        val characteristic = requireCharacteristic(service, uuid)
        return if ((characteristic.properties and BluetoothGattCharacteristic.PROPERTY_READ) != 0) {
            try {
                val response = request(
                    characteristicReadWriteChannel,
                    "Read characteristic $uuid failed."
                ) { readCharacteristic(characteristic) }
                validate(response.status, response.value, "readCharacteristic")
            } catch (_: SecurityException) {
                throw BluetoothPermissionException()
            }
        } else {
            throw GattException("Characteristic $uuid cannot be read.")
        }
    }

    private suspend fun write(
        service: String,
        uuid: String,
        command: ByteArray
    ): ByteArray {
        val characteristic = requireCharacteristic(service, uuid)
        val properties = characteristic.properties
        characteristic.writeType = when {
            (properties and BluetoothGattCharacteristic.PROPERTY_WRITE) != 0 -> {
                BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
            }
            (properties and BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) != 0 -> {
                BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE
            }
            else -> throw GattException("Characteristic ${characteristic.uuid} cannot be written.")
        }
        characteristic.value = command
        return try {
            val response = request(
                characteristicReadWriteChannel,
                "Write characteristic with value ${command.toHexString()} failed."
            ) { writeCharacteristic(characteristic) }
            validate(response.status, response.value, "writeCharacteristic")
        } catch (_: SecurityException) {
            throw BluetoothPermissionException()
        }
    }

    private suspend fun write(code: AspireCommandCode): ByteArray {
        return write(type.service, type.tx, code.hexValue)
    }

    private suspend fun <T> request(
        channel: ReceiveChannel<T>,
        message: String? = null,
        operation: BluetoothGatt.() -> Boolean
    ): T = withContext(Dispatchers.Default) {
        gattMutex.withLock {
            requireGatt.operation().checkGattOperationSucceed(message)
            return@withContext withTimeoutOrNull(5000) {
                channel.receive()
            } ?: throw ConnectionTimeoutException()
        }
    }

    private fun validate(status: Int, value: ByteArray, from: String): ByteArray {
        return when (status) {
            GATT_SUCCESS -> {
                value
            }
            else -> {
                throw GattException("$from: Unexpected, $status")
            }
        }
    }

    private fun Boolean.checkGattOperationSucceed(message: String?) {
        if (this.not()) throw GattException(message)
    }

    private fun splitSegments(value: ByteArray, command: List<Byte>, size: Int): List<List<Byte>> {
        val list = value.asList()
        val segments = mutableListOf<List<Byte>>()
        var prev = 0
        for ((start, end) in list.indices.zip(list.indices.drop(size - 1))) {
            if (list.slice(start..end - size + 2) != command) continue
            segments.add(list.slice(prev..end))
            prev = end + 1
        }
        return segments
    }

    private fun convert(array: ByteArray, flags: UByte): DeviceSensorData {
        val conditions = listOf(
            Pair(0b10000000.toUByte(), DeviceDataType.ACC),
            Pair(0b01000000.toUByte(), DeviceDataType.GYRO),
            Pair(0b00100000.toUByte(), DeviceDataType.MAG),
            Pair(0b00010000.toUByte(), DeviceDataType.QUATERNION)
        )
        val milliseconds =
            array.toShort(array.size - 2, ByteOrder.BIG_ENDIAN).toUShort().toDouble() / 1000.0
        val timestamp =
            array.toInt(array.size - 6, ByteOrder.BIG_ENDIAN).toUInt().toDouble() + milliseconds
        val map = mutableMapOf<DeviceDataType, CoordinateTimeData>()
        var start = 0
        for (condition in conditions) {
            if (flags and condition.first == condition.first) {
                val x = array.toDouble(start)
                start += 4
                val y = array.toDouble(start)
                start += 4
                val z = array.toDouble(start)
                start += 4
                val sample = if (condition.first == 0b00010000.toUByte()) {
                    val w = array.toDouble(start)
                    start += 4
                    CoordinateTimeData(CoordinateData(x, y, z, w), timestamp)
                } else {
                    CoordinateTimeData(CoordinateData(x, y, z), timestamp)
                }
                map[condition.second] = sample
            }
        }
        return DeviceSensorData(
            map[DeviceDataType.ACC],
            map[DeviceDataType.GYRO],
            map[DeviceDataType.MAG],
            map[DeviceDataType.QUATERNION]
        )
    }
}

enum class AspireCommandCode(val hexValue: ByteArray) {

    RESET(ubyteArrayOf(0xFEU, 0x02U).toByteArray()),

    SET_ACCELEROMETER_ON(ubyteArrayOf(0xF1U, 0xD0U).toByteArray()),
    SET_ACCELEROMETER_OFF(ubyteArrayOf(0xF1U, 0xDDU).toByteArray()),

    SET_GYRO_ON(ubyteArrayOf(0xF2U, 0xD0U).toByteArray()),
    SET_GYRO_OFF(ubyteArrayOf(0xF2U, 0xDDU).toByteArray()),

    SET_MAGNETOMETER_ON(ubyteArrayOf(0xF6U, 0xD0U).toByteArray()),
    SET_MAGNETOMETER_OFF(ubyteArrayOf(0xF6U, 0xDDU).toByteArray()),

    SET_QUATERNION_ON(ubyteArrayOf(0xF7U, 0xD0U).toByteArray()),
    SET_QUATERNION_OFF(ubyteArrayOf(0xF7U, 0xDDU).toByteArray()),

    SET_LINEAR_ACCELERATION_ON(ubyteArrayOf(0xF8U, 0xD0U).toByteArray()),
    SET_LINEAR_ACCELERATION_OFF(ubyteArrayOf(0xF8U, 0xDDU).toByteArray()),

    SET_SENSOR_FUSION_ON(ubyteArrayOf(0xF7U, 0xD1U).toByteArray()),
    SET_SENSOR_FUSION_OFF(ubyteArrayOf(0xF7U, 0xDEU).toByteArray()),

    SET_TX_POWER_0(ubyteArrayOf(0xF9U, 0xD1U).toByteArray()),
    SET_TX_POWER_1(ubyteArrayOf(0xF9U, 0xD2U).toByteArray()),
    SET_TX_POWER_2(ubyteArrayOf(0xF9U, 0xD3U).toByteArray()),
    SET_TX_POWER_3(ubyteArrayOf(0xF9U, 0xD4U).toByteArray()),
    SET_TX_POWER_4(ubyteArrayOf(0xF9U, 0xD5U).toByteArray()),

    CHECK_LOG_ENTRIES(ubyteArrayOf(0xFEU, 0xCEU).toByteArray()),
    RESET_LOG_ENTRIES(ubyteArrayOf(0xCEU, 0xCEU).toByteArray()),

    SET_STREAMING_ON(ubyteArrayOf(0xF3U, 0xD0U).toByteArray()),
    SET_STREAMING_OFF(ubyteArrayOf(0xF3U, 0xDDU).toByteArray()),

    SET_LOGGING_ON(ubyteArrayOf(0xF3U, 0xD1U).toByteArray()),
    SET_LOGGING_OFF(ubyteArrayOf(0xF4U, 0xD1U).toByteArray()),

    SET_DOWNLOAD_ON(ubyteArrayOf(0xF5U, 0xD1U).toByteArray()),
    SET_DOWNLOAD_OFF(ubyteArrayOf(0xF5U, 0xDEU).toByteArray()),

    SET_ACCELEROMETER_RANGE_2G(ubyteArrayOf(0xF1U, 0xD1U).toByteArray()),
    SET_ACCELEROMETER_RANGE_4G(ubyteArrayOf(0xF1U, 0xD2U).toByteArray()),
    SET_ACCELEROMETER_RANGE_8G(ubyteArrayOf(0xF1U, 0xD3U).toByteArray()),
    SET_ACCELEROMETER_RANGE_16G(ubyteArrayOf(0xF1U, 0xD4U).toByteArray()),

    SET_ACCELEROMETER_ODR_25HZ(ubyteArrayOf(0xF1U, 0xD5U).toByteArray()),
    SET_ACCELEROMETER_ODR_50HZ(ubyteArrayOf(0xF1U, 0xD6U).toByteArray()),
    SET_ACCELEROMETER_ODR_100HZ(ubyteArrayOf(0xF1U, 0xD7U).toByteArray()),

    SET_GYRO_RANGE_125DPS(ubyteArrayOf(0xF2U, 0xD1U).toByteArray()),
    SET_GYRO_RANGE_250DPS(ubyteArrayOf(0xF2U, 0xD2U).toByteArray()),
    SET_GYRO_RANGE_500DPS(ubyteArrayOf(0xF2U, 0xD3U).toByteArray()),
    SET_GYRO_RANGE_1000DPS(ubyteArrayOf(0xF2U, 0xD4U).toByteArray()),
    SET_GYRO_RANGE_2000DPS(ubyteArrayOf(0xF2U, 0xD5U).toByteArray()),

    SET_GYRO_ODR_25HZ(ubyteArrayOf(0xF2U, 0xD6U).toByteArray()),
    SET_GYRO_ODR_50HZ(ubyteArrayOf(0xF2U, 0xD7U).toByteArray()),
    SET_GYRO_ODR_100HZ(ubyteArrayOf(0xF2U, 0xD8U).toByteArray()),

    SET_MAGNETOMETER_LOW_POWER(ubyteArrayOf(0xF6U, 0xD1U).toByteArray()),
    SET_MAGNETOMETER_REGULAR(ubyteArrayOf(0xF6U, 0xD2U).toByteArray()),
    SET_MAGNETOMETER_ENHANCED_REGULAR(ubyteArrayOf(0xF6U, 0xD3U).toByteArray()),
    SET_MAGNETOMETER_HIGH_ACCURACY(ubyteArrayOf(0xF6U, 0xD4U).toByteArray()),

    SET_LED_HIGH_TIME(ubyteArrayOf(0xFAU, 0xD0U).toByteArray()),
    SET_LED_PULSE_DURATION(ubyteArrayOf(0xFAU, 0xD3U).toByteArray()),
    SET_LED_REPEAT_COUNT(ubyteArrayOf(0xFAU, 0xD6U).toByteArray()),
    PLAY_LED(ubyteArrayOf(0xFAU, 0xD9U).toByteArray()),
    STOP_LED(ubyteArrayOf(0xFAU, 0xDAU).toByteArray()),
    RESET_LED(ubyteArrayOf(0xFAU, 0xDBU).toByteArray()),

    CALIBRATE_STATUS(ubyteArrayOf(0xFEU, 0xC1U).toByteArray()),
}

enum class LedColorCode(val hexValue: Byte) {
    BLUE(0x01),
    RED(0x02),
    GREEN(0x03),
}

enum class CalibrationStatus(val hexValue: ByteArray) {
    NONE(ubyteArrayOf(0xC2U, 0x20U, 0x00U, 0x00U).toByteArray()),
    CALIBRATED(ubyteArrayOf(0xC2U, 0x20U, 0x00U, 0x03U).toByteArray()),
}

fun ByteArray.toHexString() = joinToString("") { "%02X".format(it) }

fun UInt.toByteArray(): ByteArray {
    return ByteArray(4) { i -> (this shr ((3 - i) * 8)).toByte() }
}

fun ByteArray.toInt(offset: Int = 0, bo: ByteOrder = ByteOrder.LITTLE_ENDIAN): Int {
    return ByteBuffer.wrap(this, offset, 4).order(bo).int
}

fun ByteArray.toShort(offset: Int = 0, bo: ByteOrder = ByteOrder.LITTLE_ENDIAN): Short {
    return ByteBuffer.wrap(this, offset, 2).order(bo).short
}

fun ByteArray.toDouble(offset: Int = 0, bo: ByteOrder = ByteOrder.LITTLE_ENDIAN): Double {
    return ByteBuffer.wrap(this, offset, 4).order(bo).float.toDouble()
}

val ByteArray.isCalibratedCommand: Boolean
    get() {
        return contentEquals(CalibrationStatus.CALIBRATED.hexValue)
    }

val ByteArray.isNotCalibratedCommand: Boolean
    get() {
        return contentEquals(CalibrationStatus.NONE.hexValue)
    }

val ByteArray.isCheckLogEntriesCommand: Boolean
    get() {
        return size == 6 && takeLast(2) == AspireCommandCode.CHECK_LOG_ENTRIES.hexValue.asList()
    }

val ByteArray.isSetStreamOnCommand: Boolean
    get() {
        return dropLast(1).takeLast(2) == AspireCommandCode.SET_STREAMING_ON.hexValue.asList()
    }

val ByteArray.isSetDownloadOnCommand: Boolean
    get() {
        return dropLast(5).takeLast(2) == AspireCommandCode.SET_DOWNLOAD_ON.hexValue.asList()
    }