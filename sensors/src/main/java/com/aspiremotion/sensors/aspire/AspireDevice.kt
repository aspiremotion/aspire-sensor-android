/**
 * AspireDevice.kt
 *
 * Copyright 2016-2022 Booguu Company Limited. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights granted under the terms of a software
 * license agreement between the user who downloaded the software, his/her employer (which must be your
 * employer) and Booguu Co. Ltd., (the "License").  You may not use this Software unless you agree to abide by the
 * terms of the License which can be found at www.booguu.bio/licenseterms.  The License limits your use, and you
 * acknowledge, that the Software may be modified, copied, and distributed when used in conjunction with an
 * Booguu Co. Ltd., product.  Other than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this Software and/or its documentation for any
 * purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY
 * OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL BOOGUU OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT,
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software, contact Booguu via email:
 * info@booguu.bio.
 */

package com.aspiremotion.sensors.aspire

import com.aspiremotion.sensors.*
import com.aspiremotion.sensors.CoordinateData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.onEach

class AspireDevice(override val name: String?, private val board: AspireBoard) : Device {

    private var total: Int = 0

    override var macAddress: String = board.address
        private set

    override val state: SharedFlow<DeviceConnectionState>
        get() = board.state

    override val isDisconnected: Flow<DeviceConnectionState>
        get() = board.isDisconnected

    override fun toString(): String {
        return "AspireSensor[${board.address}]"
    }

    override fun hashCode(): Int {
        return macAddress.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return hashCode() == other?.hashCode()
    }

    override suspend fun connect(timeout: Long) {
        board.connect(timeout)
    }

    override suspend fun disconnect() {
        board.disconnect()
    }

    override suspend fun getDeviceInfo(attributes: Array<DeviceInfoAttribute>): DeviceInfo {
        val info = DeviceInfo()
        board.connect(5)
        for (attribute in attributes) {
            when (attribute) {
                DeviceInfoAttribute.MAC_ADDRESS -> { info.macAddress = board.address }
                DeviceInfoAttribute.FIRMWARE_VERSION -> { info.firmwareVersion = board.getFirmwareRevision() }
                DeviceInfoAttribute.BATTERY_LEVEL -> { info.batteryLevel = board.getBatteryLevel() }
                DeviceInfoAttribute.ORIENTATION -> { info.orientation = getOrientation() }
            }
        }
        return info
    }

    override suspend fun configure(config: DeviceConfig) {
        board.connect(5)
        board.configureTxPower(4)
        if (config is QuaternionDeviceConfig) {
            config.accConfig?.apply { board.configureAccelerometer(odr, range) }
            config.gyroConfig?.apply { board.configureGyro(odr, range) }
            board.configureModules(accelerometer = config.acc, gyro = config.gyro, magnetometer = config.mag)
            board.configureQuaternionStream(true)
            board.configureSensorFusion(true)
        } else {
            board.configureSensorFusion(false)
            board.configureQuaternionStream(false)
            config.accConfig?.apply { board.configureAccelerometer(odr, range) }
            config.gyroConfig?.apply { board.configureGyro(odr, range) }
            config.magConfig?.apply { board.configureMagnetometer(preset) }
            board.configureModules(config.accConfig != null, config.gyroConfig != null, config.magConfig != null)
        }
    }

    override suspend fun startStreaming() {
        board.setStreaming(true)
    }

    override suspend fun stopStreaming() {
        board.setStreaming(false)
        board.disableModules()
    }

    override suspend fun startLogging() {
        if (board.state.firstOrNull() != DeviceConnectionState.CONNECTED) throw UnexpectedDisconnectionException()
        board.setLogging(true)
    }

    override suspend fun stopLogging() {
        total = 0
        board.connect(5)
        board.setLogging(false)
        board.disableModules()
        total = board.checkLogEntries()
        board.setDownload(true)
    }

    override suspend fun cancelDownload() {
        try {
            board.setDownload(false)
            board.resetLogEntries()
        } finally {
            board.disconnect()
        }
        board.isDisconnected.first()
    }

    override suspend fun playLed(color: DeviceLedColor, repeat: Int) {
        board.connect(5)
        board.resetLed()
        board.configureLedPattern(
            when (color) {
                DeviceLedColor.RED -> LedColorCode.RED
                DeviceLedColor.GREEN -> LedColorCode.GREEN
                DeviceLedColor.BLUE -> LedColorCode.BLUE
            },
            500U,
            300U,
            repeat.toUInt()
        )
        board.playLed()
    }

    override suspend fun reset() {
        board.connect(5)
        board.reset()
        board.isDisconnected.first()
    }

    override suspend fun isCalibrated(): Boolean {
        board.connect(5)
        return board.readCalibrationStatus()
    }

    override fun receiveStream(): Flow<DeviceSensorData> {
        return board.receiveStream()
    }

    override fun receiveDownload(): Flow<DeviceData> = callbackFlow {
        board.receiveDownload().onEach {
            send(it.first)
            send(DownloadProgress(total, it.second))
            if (it.second == 0) { close() }
        }.collect()
    }

    private suspend fun getOrientation(): CoordinateData {
        board.configureAccelerometer(100, 4)
        board.configureModules(accelerometer = true, gyro = false, magnetometer = false)
        board.configureQuaternionStream(false)
        board.configureSensorFusion(false)
        board.setStreaming(true)
        val orientation = board.receiveStream().first().acceleration!!.value
        board.setStreaming(false)
        board.disableModules()
        return orientation
    }
}


