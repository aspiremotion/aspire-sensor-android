/**
 * BluetoothPermissionActivity.kt
 *
 * Copyright 2016-2022 Booguu Company Limited. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights granted under the terms of a software
 * license agreement between the user who downloaded the software, his/her employer (which must be your
 * employer) and Booguu Co. Ltd., (the "License").  You may not use this Software unless you agree to abide by the
 * terms of the License which can be found at www.booguu.bio/licenseterms.  The License limits your use, and you
 * acknowledge, that the Software may be modified, copied, and distributed when used in conjunction with an
 * Booguu Co. Ltd., product.  Other than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this Software and/or its documentation for any
 * purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY
 * OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL BOOGUU OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT,
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software, contact Booguu via email:
 * info@booguu.bio.
 */

package com.aspiremotion.sensors

import android.Manifest
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.provider.Settings
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.trySendBlocking
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * Abstract base class to support all Activities that depend on
 * Bluetooth feature to complete its use case.
 *
 * This class will handles all the required Bluetooth permission
 * checks, prompting users to enable/grant missing items as appropriate.
 */
abstract class BluetoothPermissionActivity : AppCompatActivity() {

    private val channel = Channel<Boolean>(capacity = 1)

    private val intentActivityResultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            channel.trySendBlocking(result.resultCode == RESULT_OK)
        }

    private val permissionsActivityResultLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { entries ->
            channel.trySendBlocking(entries.values.fold(true) { acc, b -> acc && b })
        }

    /**
     * 1. Check device BLUETOOTH_SCAN and BLUETOOTH_CONNECT permissions. Notify user with supplied
     * [permissionsDialog] if available and then start permissions request.
     *
     * 2. Ensure Bluetooth Adapter is enabled. Notify user with supplied [bluetoothAdapterDialog] if
     * available and then request user to enable BluetoothAdapter.
     *
     * 3. Ensure location service is enabled if it's Android 11 or earlier. Notify user with
     * supplied [locationServiceDialog] if available and then request user to enable location
     * service.
     *
     * @param permissionsDialog Dialog to inform user why they need to grant Bluetooth permissions
     *
     * @param bluetoothAdapterDialog Dialog to inform user why they need to enable Bluetooth adapter
     *
     * @param locationServiceDialog Dialog to inform user why they need to enable location service
     *
     * @return Returns true when all required services are enabled.
     */
    open suspend fun startBluetoothPermissionsCheck(
        permissionsDialog: AlertDialog.Builder? = null,
        bluetoothAdapterDialog: AlertDialog.Builder? = null,
        locationServiceDialog: AlertDialog.Builder? = null
    ): Boolean {
        val granted = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
                checkSelfPermission(Manifest.permission.BLUETOOTH_SCAN) == PackageManager.PERMISSION_GRANTED &&
                        checkSelfPermission(Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_GRANTED
            }
            else -> {
                checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            }
        }
        if (granted) {
            return promptEnableBluetooth(bluetoothAdapterDialog, locationServiceDialog)
        }

        permissionsDialog?.apply { showAlertDialog(this) }
        permissionsActivityResultLauncher.launch(
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                arrayOf(
                    Manifest.permission.BLUETOOTH_SCAN,
                    Manifest.permission.BLUETOOTH_CONNECT
                )
            } else {
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            }
        )
        return channel.receive() && promptEnableBluetooth(bluetoothAdapterDialog, locationServiceDialog)
    }

    private suspend fun promptEnableBluetooth(
        bluetoothAdapterDialog: AlertDialog.Builder?,
        locationServiceDialog: AlertDialog.Builder?
    ): Boolean {
        val adapter = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
                (getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
            }
            else -> {
                @Suppress("DEPRECATION")
                BluetoothAdapter.getDefaultAdapter()
            }
        }
        return adapter?.let {
            if (adapter.isEnabled) {
                promptEnableLocationService(locationServiceDialog)
            } else {
                bluetoothAdapterDialog?.apply { showAlertDialog(this) }
                intentActivityResultLauncher.launch(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
                channel.receive()
            }
        } ?: true
    }

    private suspend fun promptEnableLocationService(
        locationServiceDialog: AlertDialog.Builder?
    ): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            true
        } else if ((getSystemService(Context.LOCATION_SERVICE) as LocationManager)
                .isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            true
        } else {
            locationServiceDialog?.apply { showAlertDialog(this) }
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            false
        }
    }

    private suspend fun showAlertDialog(builder: AlertDialog.Builder) =
        suspendCoroutine { continuation ->
            builder.setOnDismissListener { continuation.resume(Unit) }.create().show()
        }
}