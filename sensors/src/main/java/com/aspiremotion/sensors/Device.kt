/**
 * Device.kt
 *
 * Copyright 2016-2022 Booguu Company Limited. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights granted under the terms of a software
 * license agreement between the user who downloaded the software, his/her employer (which must be your
 * employer) and Booguu Co. Ltd., (the "License").  You may not use this Software unless you agree to abide by the
 * terms of the License which can be found at www.booguu.bio/licenseterms.  The License limits your use, and you
 * acknowledge, that the Software may be modified, copied, and distributed when used in conjunction with an
 * Booguu Co. Ltd., product.  Other than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this Software and/or its documentation for any
 * purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY
 * OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL BOOGUU OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT,
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software, contact Booguu via email:
 * info@booguu.bio.
 */

package com.aspiremotion.sensors

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharedFlow

/**
 * Device interface corresponds a physical BLE device. It contains
 * functions to communicate with the device.
 *
 * @property macAddress MAC address of the device
 *
 * @property name BLE device name retrieved from BluetoothDevice
 *
 * @property state Flow of current device connection state
 *
 * @property isDisconnected Flow to monitor device disconnection
 */
interface Device {

    val macAddress: String

    val name: String?

    val state: SharedFlow<DeviceConnectionState>

    val isDisconnected: Flow<DeviceConnectionState>

    /**
     * Connect to the device or throws [ConnectionTimeoutException] after specified timeout.
     *
     * @param timeout Timeout in seconds.
     */
    suspend fun connect(timeout: Long = 10)

    /**
     * Disconnect from device.
     */
    suspend fun disconnect()

    /**
     * Connect to the device if not connected.
     *
     * Retrieve attributes requested and return the result.
     *
     * @param attributes Attributes to be retrieved.
     *
     * @return DeviceInfo object containing the requested attributes.
     */
    suspend fun getDeviceInfo(attributes: Array<DeviceInfoAttribute>): DeviceInfo

    /**
     * Connect to the device if not connected.
     *
     * Configure the device based on DeviceConfig settings.
     *
     * @param config Configuration of the modules.
     */
    suspend fun configure(config: DeviceConfig)

    /**
     * Start streaming. This function should only be called after device is connected and
     * configured using [configure].
     *
     * Results can be retrieved by subscribing to [receiveStream] flow.
     */
    suspend fun startStreaming()

    /**
     * Stop streaming and disconnect.
     */
    suspend fun stopStreaming()

    /**
     * Start logging. This function should only be called after device is connected and configured
     * using [configure]. The function returns after logging is started successfully on the device.
     * After started, the device can be disconnected if needed and logging will continue.
     */
    suspend fun startLogging()

    /**
     * Connect to the device if not connected.
     *
     * Stop logging and start downloading.
     *
     * Results can be retrieved by subscribing to [receiveDownload] flow which returns download data
     * [DeviceSensorData] and download progress [DownloadProgress].
     */
    suspend fun stopLogging()

    /**
     * Stop download and clear all log entries on the device. Disconnect after log is cleared.
     */
    suspend fun cancelDownload()

    /**
     * Connect to the device if not connected.
     *
     * Lit LED color [repeat] times.
     *
     * @param color Color of the LED.
     *
     * @param repeat Number of times LED should be lit.
     */
    suspend fun playLed(color: DeviceLedColor, repeat: Int)

    /**
     * Connect to the device if not connected.
     *
     * Clear all log entries, reset device to factory state and disconnect.
     */
    suspend fun reset()

    /**
     * Connect to the device if not connected.
     *
     * Check device calibration status.
     *
     * @return true if device is calibrated.
     */
    suspend fun isCalibrated(): Boolean

    /**
     * @return Flow for streaming data.
     */
    fun receiveStream(): Flow<DeviceSensorData>

    /**
     * @return Flow for download logged data and progress. Cast [DeviceData] to [DeviceSensorData]
     * for logged data or [DownloadProgress] for current download progress.
     */
    fun receiveDownload(): Flow<DeviceData>
}