/**
 * MainActivity.kt
 *
 * Copyright 2016-2022 Booguu Company Limited. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights granted under the terms of a software
 * license agreement between the user who downloaded the software, his/her employer (which must be your
 * employer) and Booguu Co. Ltd., (the "License").  You may not use this Software unless you agree to abide by the
 * terms of the License which can be found at www.booguu.bio/licenseterms.  The License limits your use, and you
 * acknowledge, that the Software may be modified, copied, and distributed when used in conjunction with an
 * Booguu Co. Ltd., product.  Other than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this Software and/or its documentation for any
 * purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY
 * OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL BOOGUU OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT,
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software, contact Booguu via email:
 * info@booguu.bio.
 */

package com.aspiremotion.demo

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.FileProvider
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewModelScope
import com.aspiremotion.demo.databinding.ActivityMainBinding
import com.aspiremotion.sensors.*
import com.aspiremotion.sensors.BuildConfig
import com.aspiremotion.sensors.CoordinateTimeData
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class MainActivity : BluetoothPermissionActivity() {

    private val viewModel: SensorViewModel by viewModels()

    private val deviceManager: DeviceManager
        get() { return viewModel.deviceManager }

    private var device: Device?
        get() { return viewModel.device }
        set(value) { viewModel.device = value }

    private lateinit var binding: ActivityMainBinding

    private lateinit var file: File

    /**
     * Mutex to prevent race condition adding messages to text view.
     */
    private var mutex = Mutex()

    private var scanJob: Job? = null

    private var streamJob: Job? = null

    private var messages = mutableListOf<String>()

    private var exclusions = mutableSetOf<Device>()

    init {
        lifecycleScope.launch {
            // Check Bluetooth Permissions everytime Activity resumes
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                // From BluetoothPermissionActivity
                if (startBluetoothPermissionsCheck(
                        permissionsDialog = AlertDialog.Builder(this@MainActivity)
                            .setTitle(getString(R.string.permission_dialog_title))
                            .setMessage(getString(R.string.permission_dialog_message))
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, null)

                    )
                ) {
                    // Permissions granted. Enable BLE functions
                    binding.connectButton.isEnabled = streamJob?.isActive != true
                    binding.disconnectButton.isEnabled = device != null && streamJob?.isActive != true
                    binding.changeButton.isEnabled = streamJob?.isActive != true
                    binding.freq25Button.isEnabled = device != null && streamJob?.isActive != true
                    binding.freq50Button.isEnabled = device != null && streamJob?.isActive != true
                    binding.freq100Button.isEnabled = device != null && streamJob?.isActive != true
                    binding.stopButton.isEnabled = device != null && streamJob?.isActive == true
                } else {
                    // Permissions declined. Exit Activity.
                    finish()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Setup Timber Debug Logger in Debug Build only.
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        file = File.createTempFile("data", ".csv", cacheDir)
        file.deleteOnExit()

        binding.exportButton.isEnabled = false

        binding.connectButton.setOnClickListener {
            viewModel.viewModelScope.launch { connect() }
        }

        binding.disconnectButton.setOnClickListener {
            viewModel.viewModelScope.launch {
                device?.disconnect()
                binding.disconnectButton.isEnabled = false
                setButtons(true)
            }
        }

        binding.changeButton.setOnClickListener {
            lifecycleScope.launch { changeDevice() }
        }

        binding.freq25Button.setOnClickListener {
            streamJob = viewModel.viewModelScope.launchStreaming(
                DeviceConfig(
                    AccConfig(odr = 25, range = 4),
                    GyroConfig(odr = 25, range = 2000),
                    null
                )
            )
        }

        binding.freq50Button.setOnClickListener {
            streamJob = viewModel.viewModelScope.launchStreaming(
                DeviceConfig(
                    AccConfig(odr = 50, range = 4),
                    GyroConfig(odr = 50, range = 2000),
                    null
                )
            )
        }

        binding.freq100Button.setOnClickListener {
            streamJob = viewModel.viewModelScope.launchStreaming(
                DeviceConfig(
                    AccConfig(odr = 100, range = 4),
                    GyroConfig(odr = 100, range = 2000),
                    null
                )
            )
        }

        binding.stopButton.setOnClickListener {
            viewModel.viewModelScope.launch {
                streamJob?.cancelAndJoin()
            }
        }

        binding.exportButton.setOnClickListener {
            export()
        }
    }

    override fun onPause() {
        super.onPause()

        // Stop scanning in background
        if (scanJob?.isActive == true) {
            lifecycleScope.launch { stopScan() }
        }

        // Disconnect device in background if device is connected but not streaming.
        if (streamJob?.isActive != true) {
            viewModel.viewModelScope.launch {
                device?.disconnect()
                device = null
                binding.connectButton.isEnabled = true
                binding.disconnectButton.isEnabled = false
                setButtons(false)
            }
        }
    }

    private fun setButtons(enabled: Boolean) {
        binding.connectButton.isEnabled = enabled
        binding.freq25Button.isEnabled = enabled
        binding.freq50Button.isEnabled = enabled
        binding.freq100Button.isEnabled = enabled
    }

    /**
     * Connect to last connected device. If there was no devices connected start scanning for
     * closest device.
     */
    private suspend fun connect() {
        binding.changeButton.isEnabled = false
        binding.disconnectButton.isEnabled = false
        setButtons(false)
        device?.disconnect()
        device = load()?.apply { getDeviceInfo() }
        if (device == null) lifecycleScope.launchScan()
    }

    /**
     * Find a different device to connect to if there are more than one device nearby.
     */
    private suspend fun changeDevice() {
        // Scanning is active. Stop scanning and do nothing instead.
        if (scanJob?.isActive == true) {
            stopScan()
            return
        }
        binding.changeButton.isEnabled = false
        binding.disconnectButton.isEnabled = false
        setButtons(false)
        device?.disconnect()
        forget()
        exclude()
        scanJob = lifecycleScope.launchScan()
    }

    /**
     * Start scanning and find closest device.
     */
    private fun CoroutineScope.launchScan(): Job = launch {
        try {
            deviceManager.startBleScan()
            addMessage(resources.getString(R.string.start_scanning))
            updateTextView()

            binding.changeButton.isEnabled = true

            val job = deviceManager.devices.onEach {
                findAndReplaceAfterOrAdd(
                    it.first.macAddress,
                    getString(R.string.start_scanning),
                    "${it.first.macAddress}: ${it.second}"
                )
            }.launchIn(this)

            device = findClosest()

            job.cancelAndJoin()
        } finally {
            withContext(NonCancellable) {
                deviceManager.stopBleScan()
                addMessage(resources.getString(R.string.stop_scanning))
                updateTextView()
            }
        }
        // Starts getDeviceInfo in new coroutine, so scanJob can be done.
        lifecycleScope.launch { device?.getDeviceInfo() }
    }

    /**
     * Stop scanning
     */
    private suspend fun stopScan() {
        binding.changeButton.isEnabled = false
        scanJob?.cancelAndJoin()
        binding.changeButton.isEnabled = true
    }

    /**
     * Find closest device among the previously discovered devices.
     */
    private suspend fun findClosest(wait: Duration = 5.toDuration(DurationUnit.SECONDS)): Device? {
        delay(wait)
        deviceManager.devices.first()
        val ignore = deviceManager.distances.entries.count() == 1
        return deviceManager.distances.entries
            .filter { entry -> !exclusions.contains(entry.key) || ignore }
            .minByOrNull { entry -> entry.value }?.key?.apply { remember() }
    }

    /**
     * Load previously connected device from preferences.
     */
    private fun load(): Device? {
        return getSharedPreferences("sensors", Context.MODE_PRIVATE)
            .getString("rememberMe", "")
            .takeUnless { it.isNullOrEmpty() }
            ?.let { deviceManager.getDevice(it) }
    }

    /**
     * Save current connected device MAC address to preferences.
     */
    private fun Device.remember() {
        getSharedPreferences("sensors", Context.MODE_PRIVATE)
            .edit()
            .apply {
                putString("rememberMe", macAddress)
                apply()
            }
    }

    /**
     * Remove remembered device from preferences.
     */
    private fun forget() {
        getSharedPreferences("sensors", Context.MODE_PRIVATE)
            .edit()
            .apply {
                remove("rememberMe")
                apply()
            }
    }

    /**
     * Exclude current connected device from being connected again, so a different device can be
     * connected from [findClosest].
     */
    private fun exclude() {
        val device = device ?: return
        if (exclusions.containsAll(deviceManager.distances.keys.filter { key -> key != device })) {
            exclusions.clear()
        }
        exclusions.add(device)
    }

    /**
     * Connect to device and read Battery Level and Firmware Version.
     */
    private suspend fun Device.getDeviceInfo() {
        try {
            binding.changeButton.isEnabled = false
            addMessage(resources.getString(R.string.connecting, macAddress))
            updateTextView()
            val info = getDeviceInfo(
                arrayOf(
                    DeviceInfoAttribute.BATTERY_LEVEL,
                    DeviceInfoAttribute.FIRMWARE_VERSION
                )
            )
            addMessage(resources.getString(R.string.connected))
            addMessage(resources.getString(R.string.battery_level, info.batteryLevel))
            addMessage(resources.getString(R.string.firmware_revision, info.firmwareVersion))
            binding.disconnectButton.isEnabled = true
        } catch (error: Exception) {
            if (error !is CancellationException) {
                addMessage(resources.getString(R.string.connection_error, error.localizedMessage))
            }
        } finally {
            binding.changeButton.isEnabled = true
        }
        updateTextView()
        setButtons(true)
    }

    /**
     * Configure device with [config] and start streaming
     */
    private fun CoroutineScope.launchStreaming(config: DeviceConfig) = launch(Dispatchers.Main) {
        val device = device ?: return@launch

        binding.changeButton.isEnabled = false
        binding.disconnectButton.isEnabled = false
        binding.stopButton.isEnabled = false
        setButtons(false)

        val types = config.types
        val odr = config.odr

        writeHeadersToFile(odr, types)

        addMessage(resources.getString(R.string.start, odr))
        addMessage(resources.getString(R.string.count, odr, 0))
        updateTextView()

        // Subscribe to receive streaming data
        val receiveJob = launchReceiveStream(device, odr)

        try {
            // Set stream configurations
            device.configure(config)
            // Start streaming
            device.startStreaming()
            logDateTime()
            updateTextView()
            binding.stopButton.isEnabled = true
            // Wait for disconnection
            if (device.isDisconnected.first() == DeviceConnectionState.UNEXPECTED_DISCONNECTED) {
                throw UnexpectedDisconnectionException()
            }
        } catch (error: Throwable) {
            if (error !is CancellationException) {
                addMessage(resources.getString(R.string.connection_error, error.localizedMessage))
            }
        } finally {
            withContext(NonCancellable) {
                // Stop streaming and disconnect in a non-cancellable block.
                try {
                    device.stopStreaming()
                } catch (_: Throwable) { }
                device.disconnect()
                receiveJob.cancelAndJoin()
                binding.stopButton.isEnabled = false
                binding.changeButton.isEnabled = true
                binding.exportButton.isEnabled = true
                setButtons(true)
                addMessage(resources.getString(R.string.stop))
                logDateTime()
                logFileSize()
                updateTextView()
            }
        }
    }

    /**
     * Subscribe to streaming data and append result to file.
     */
    private fun CoroutineScope.launchReceiveStream(device: Device, odr: Int): Job =
        launch(Dispatchers.Main) {
            var received = 0
            var prev: Double? = null
            device.receiveStream().onEach {

                received += 1

                val acceleration = it.acceleration
                val gyro = it.gyro
                val magneticField = it.magneticField
                val quaternion = it.quaternion
                val timestamp: Double =
                    acceleration?.timestamp
                        ?: gyro?.timestamp
                        ?: magneticField?.timestamp
                        ?: quaternion?.timestamp
                        ?: 0.0

                findAndReplaceOrAdd(
                    odr.toString(),
                    resources.getString(R.string.count, odr, received)
                )

                prev?.let { prev ->
                    val gap = (odr.toDouble() / 100.0) * 1.5
                    if (timestamp - prev >= gap) {
                        addMessage(resources.getString(R.string.packet_loss, prev, timestamp))
                    }
                }
                prev = timestamp

                updateTextView()

                withContext(Dispatchers.Default) {
                    appendDataToFile(acceleration, gyro, magneticField, quaternion, timestamp)
                }
            }.collect()
        }

    /**
     * Export csv file.
     */
    private fun export() {
        if (file.length() == 0L) return
        val uri = FileProvider.getUriForFile(this, "$packageName.provider", file)
        val intent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_STREAM, uri)
            type = "text/plain"
        }
        startActivity(Intent.createChooser(intent, null))
    }

    /**
     * Log current time to text view.
     */
    private suspend fun logDateTime() {
        val time = Calendar.getInstance().time
        val formatter = SimpleDateFormat.getDateTimeInstance()
        addMessage(formatter.format(time))
    }

    /**
     * Log csv file size to text view.
     */
    private suspend fun logFileSize() {
        val b = file.length().toDouble()
        if (b < 1024.0) {
            addMessage(String.format("%.0f B", b))
            return
        }
        val kb = b / 1024.0
        if (kb < 1024.0) {
            addMessage(String.format("%.0f KB", kb))
            return
        }
        val mb = kb / 1024.0
        if (mb < 1024.0) {
            addMessage(String.format("%.0f MB", mb))
            return
        }
        val gb = mb / 1024.0
        addMessage(String.format("%.0f GB", gb))
    }

    /**
     * Overwrite csv file with csv column headers.
     */
    private fun writeHeadersToFile(odr: Int, types: List<DeviceDataType>) {
        val headers = mutableListOf(getString(R.string.odr, odr))
        if (types.contains(DeviceDataType.ACC)) {
            headers.addAll(listOf("ax", "ay", "az"))
        }
        if (types.contains(DeviceDataType.GYRO)) {
            headers.addAll(listOf("gx", "gy", "gz"))
        }
        if (types.contains(DeviceDataType.MAG)) {
            headers.addAll(listOf("mx", "my", "mz"))
        }
        if (types.contains(DeviceDataType.QUATERNION)) {
            headers.addAll(listOf("qx", "qy", "qz", "qw"))
        }
        file.writeText(headers.joinToString(postfix = "\n"))
    }

    /**
     * Append received device data to csv file.
     */
    private fun appendDataToFile(
        acceleration: CoordinateTimeData?,
        gyro: CoordinateTimeData?,
        magneticField: CoordinateTimeData?,
        quaternion: CoordinateTimeData?,
        timestamp: Double
    ) {
        val list = mutableListOf<String>()
        list.add("$timestamp")
        if (acceleration != null) {
            list.add("${acceleration.value.x}")
            list.add("${acceleration.value.y}")
            list.add("${acceleration.value.z}")
        }
        if (gyro != null) {
            list.add("${gyro.value.x}")
            list.add("${gyro.value.y}")
            list.add("${gyro.value.z}")
        }
        if (magneticField != null) {
            list.add("${magneticField.value.x}")
            list.add("${magneticField.value.y}")
            list.add("${magneticField.value.z}")
        }
        if (quaternion != null) {
            list.add("${quaternion.value.x}")
            list.add("${quaternion.value.y}")
            list.add("${quaternion.value.z}")
            list.add("${quaternion.value.w}")
        }
        file.appendText(list.joinToString(postfix = "\n"))
    }

    /**
     * Helper function that adds a message to first line of text view.
     */
    private suspend fun addMessage(message: String) {
        mutex.withLock {
            messages.add(0, message)
        }
    }

    /**
     * Helper function that replaces a message at specified index.
     */
    private suspend fun replaceMessage(index: Int, message: String) {
        mutex.withLock {
            messages[index] = message
        }
    }

    /**
     * Helper function that finds first index of messages that starts with [filter].
     */
    private suspend fun findMessageStartsWith(filter: String): Int {
        return mutex.withLock {
            messages.indexOfFirst { it.startsWith(filter) }
        }
    }

    /**
     * Helper function that replaces a message that starts with [filter] or adds the message to the
     * first line of text view.
     */
    private suspend fun findAndReplaceOrAdd(filter: String, text: String) {
        val index = findMessageStartsWith(filter)
        if (index == -1) {
            addMessage(text)
        } else {
            replaceMessage(index, text)
        }
    }

    /**
     * Helper function that replaces a message that starts with [filter] only if it appears above the
     * index of [after], or adds the message to the first line of text view.
     */
    private suspend fun findAndReplaceAfterOrAdd(filter: String, after: String, text: String) {
        val offset = messages.indexOf(after)
        val index = findMessageStartsWith(filter)
        if (index == -1 || index > offset) {
            messages.add(0, text)
        } else {
            messages[index] = text
        }
        updateTextView()
    }

    /**
     * Helper function that updates the text view.
     */
    private suspend fun updateTextView() {
        mutex.withLock {
            binding.textView.text = messages.joinToString("\n")
        }
    }
}