# Aspire Sensor SDK - Android

### Requirement

Android Studio 2021.3.1 or greater

Aspire Motion Sensor Board

### How to Use

### Kotlin Coroutines Dependencies

```
dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.4")
}
```

### Device Manager Interface

**DeviceManager** is the starting point of using Aspire devices.

Use AspireBalanceDeviceManager to start scanning devices or connecting to known device using MAC
address.

### Scanning Example

You can start scanning by calling **startBleScan()** and subscribe to **devices** flow to retrieve
discovered devices. Cancel the job to stop scanning.

```
val manager: DeviceManager = AspireBalanceDeviceManager(
    (context.getSystemService(Context.BLUETOOTH_SERVICE) as? BluetoothManager)?.adapter,
    context
)

val job = launch {
    try {
        // Start BLE Scanning
        manager.startBleScan()

        manager.devices.onEach { device ->
            // Do Work With Discovered Device
        }.collect()
    } finally {
        // Stop BLE Scanning after Coroutine job is cancelled.
        withContext(NonCancellable) {
            manager.stopBleScan()
        }
    }
}
```

### Device Interface

**Device** is how you interact with the device. Most functions will connect to the device automatically and disconnect as needed.
If the job that connects to the device is cancelled, the device will be disconnected automatically. It is recommended to use
Android's lifecycle-aware components to maintain connection such as ViewModelScope or LifecycleScope.

```
// Find device using MAC address
val device = manager.getDevice("MAC_ADDRESS_HERE")

// Auto connect if disconnected and get battery level & firmware version
val info = device.getDeviceInfo(
    arrayOf(
        DeviceInfoAttribute.BATTERY_LEVEL,
        DeviceInfoAttribute.FIRMWARE_VERSION
    )
)

// Auto connect if disconnected and set device configuration. Set Accelerometer to 100 Hz, 4G and Gyroscope to 100 Hz, 2000 dps.
val config = DeviceConfig(AccConfig(100, 4), GyroConfig(100, 2000), null)
device.configure(config)

// Initiate streaming. This DO NOT auto connect. Device needs to be connected before starting.
val streamJob = launch {
    
    // Launch a new coroutine to wait & receive data stream without blocking
    val receiveJob = device.receiveStream().onEach {
        // Do Work with Received Acc/Gyro Data.
    }.launchIn(CoroutineScope(Dispatchers.Main))
    
    try {
        device.configure(config)
        device.startStreaming()
        // Streaming started...
        // Suspend coroutine to wait for disconnection.
        if (device.isDisconnected.first() == DeviceConnectionState.UNEXPECTED_DISCONNECTED) {
            throw UnexpectedDisconnectionException()
        }
        // Do Work After Disconnected.
    } catch (error: Exception) {
        if (error !is CancellationException) {
            // Something went wrong. Device lost connection.
        }
    } finally {
        withContext(NonCancellable) {
            // Stop streaming and disconnect in a non-cancellable block.
            try {
                // Device will throw exception if already disconnected. Catch it here.
                device.stopStreaming()
            } catch (_: Throwable) { }
            device.disconnect()
            receiveJob.cancelAndJoin()
        }
    }
}
```

### BluetoothPermissionActivity

**BluetoothPermissionActivity** is an abstract class that helps simplify Bluetooth permission checks. Activity class that implements
BluetoothPermissionActivity can call ´startBluetoothPermissionsCheck´ to initiate permission check process. Alert dialogs can also
be included to inform user of why specific permissions are needed.

```
class MainActivity : BluetoothPermissionActivity() {
    ......
    init {
        lifecycleScope.launch {
            // Check Bluetooth Permissions everytime Activity resumes in case user made any settings change, e.g. Airplane mode
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                if (startBluetoothPermissionsCheck(
                        permissionsDialog = AlertDialog.Builder(this@MainActivity)
                            .setTitle("Permission Dialog Title")
                            .setMessage("Permission Request Message")
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, null)

                    )
                ) {
                    // Permisions Granted. BLE related functions can be used.
                } else {
                    // Exit the activity if permissions are not granted.
                    finish()
                }
            }
        }
    }
    .......
}

```
